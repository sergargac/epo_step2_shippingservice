package com.epo.trainingproject.shippingservice.service.impl;

import com.epo.trainingproject.shippingservice.service.ShippingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShippingServiceImpl implements ShippingService {

    Logger logger = LoggerFactory.getLogger(ShippingServiceImpl.class);

    @Override
    public Integer calculateTag(List<Integer> ids) {
        //String uuid =  UUID.randomUUID().toString();
        int code = ids.hashCode();
        logger.info("Hashcode generated: " + code);
        return 200;
    }
}
