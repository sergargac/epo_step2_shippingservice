package com.epo.trainingproject.shippingservice.service;

import java.util.List;

public interface ShippingService {

    public Integer calculateTag(List<Integer> ids);

}
