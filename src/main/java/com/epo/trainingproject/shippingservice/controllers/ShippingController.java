package com.epo.trainingproject.shippingservice.controllers;

import com.epo.trainingproject.shippingservice.service.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ShippingController {

    @Autowired
    ShippingService shippingService;

    @PostMapping("shipping/ship")
    public Integer shipOrder(@RequestBody List<Integer> ids){
        return shippingService.calculateTag(ids);
    }

}
